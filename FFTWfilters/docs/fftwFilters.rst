===================
 FFTW-Filter
===================

=============== ========================================================================================================
**Summary**:    :pluginsummary:`FFTW-Filter`
**Type**:       :plugintype:`FFTW-Filter`
**License**:    :pluginlicense:`FFTW-Filter`
**Platforms**:  Windows, Linux
**Author**:     :pluginauthor:`FFTW-Filter`
=============== ========================================================================================================
  
Overview
========

.. pluginsummaryextended::
    :plugin: FFTW-Filter

These filters are defined in the plugin:

.. pluginfilterlist::
    :plugin: FFTW-Filter
    :overviewonly:

Filters
==============
        
Detailed overview about all defined filters:
    
.. pluginfilterlist::
    :plugin: FFTW-Filter

Changelog
=========

* itom setup 2.2.1: This plugin has been compiled using the FFTW 3.3.5
* itom setup 3.0.0: This plugin has been compiled using the FFTW 3.3.5
* itom setup 3.1.0: This plugin has been compiled using the FFTW 3.3.5
* itom setup 3.2.1: This plugin has been compiled using the FFTW 3.3.5
* itom setup 4.0.0: This plugin has been compiled using the FFTW 3.3.5
